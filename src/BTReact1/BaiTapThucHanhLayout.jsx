import React from 'react'
import Header from './Header'
import Banner from './Banner'
import Item from './Item'
import Footer from './Footer'

const BaiTapThucHanhLayout = () => {
  return (
    <div>
        <div>
        <Header />
        </div>
        <Banner />
        <Item />
        <Footer />
    </div>
  )
}

export default BaiTapThucHanhLayout
